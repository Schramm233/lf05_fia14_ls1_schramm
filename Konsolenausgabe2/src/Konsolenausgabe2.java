
public class Konsolenausgabe2 {
	
	
	public static void main(String[] args) {
		String Stern 	= "*";
		String Stern2 	= "**";

		int F1 = -20, F2 = -10, F3 = 0, F4 = 20, F5 = 30;
		double C1 = -28.8889, C2 = -23.3333, C3 = -17.7778, C4 = -6.6667, C5 = -1.1111;
		
		System.out.println("Aufgabe 1 \n");
		
		System.out.printf( "%5s\n", Stern2); 	
		System.out.printf( "%-3s  %3s\n", Stern,Stern); 	
		System.out.printf( "%-3s  %3s\n", Stern,Stern); 	
		System.out.printf( "%5s\n", Stern2 ); 
		

		
	
		System.out.println("\n Aufgabe 2 \n");
		
		System.out.printf( "%-5s %-19s %s %4s\n", "0!","=","=","1");
		System.out.printf( "%-5s %-19s %s %4s\n", "1!","=1","=","1");
		System.out.printf( "%-5s %-19s %s %4s\n", "2!","=1*2","=","2");
		System.out.printf( "%-5s %-19s %s %4s\n", "3!","=1*2*3","=","6");
		System.out.printf( "%-5s %-19s %s %4s\n", "4!","=1*2*3*4","=","24");
		System.out.printf( "%-5s %-19s %s %4s\n", "5!","=1*2*3*4*5","=","120");

		System.out.println("\n Aufgabe 3 \n");

		System.out.printf( "%-12s | %10s \n", "Fahrenheit","Celsius");
		System.out.printf( "%-12s | %10s \n", F1,C1);
		System.out.printf( "%-12s | %10s \n", F2,C2);
		System.out.printf( "%-12s | %10s \n", F3,C3);
		System.out.printf( "%-12s | %10s \n", F4,C4);
		System.out.printf( "%-12s | %10s \n", F5,C5);

		
		
		
	
	}
}
