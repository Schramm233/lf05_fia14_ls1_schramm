
public class Konsolenausgabe {
	
	public static void main(String[] args) {


		System.out.print("dies ist ein test");
		System.out.println("dies ist ein test 2");
		
		
		String Nomen 	= "Beispielsätze";
		String Stern 	= "  *";
		String Stern2 	= " ** ";
		String Stern3 	= " *** ";
		String Stern5 	= "***** ";		
		
		double a	= 22.4234234;
		double b 	= 111.2222;
		double c	= 4.0;
		double d 	= 1000000.551;
		double e	= 97.34;
		
		
		System.out.println("\n Aufgabe 1 \n");
		
		System.out.print("Beispielsätze überfordern meine Kreativität.");
		System.out.print("Hoffe einfach das dies reicht.");
		System.out.println();
		System.out.print("\"" + Nomen + "\" überfordern meine Kreativität. \n");
		System.out.println("Hoffe einfach das dies reicht\\genügt.");
		
		System.out.println(" \n Aufgabe 2 \n");

		
		System.out.println(Stern);
		System.out.println(Stern + Stern + Stern);
		System.out.println(Stern + Stern + Stern + Stern);
		System.out.println(Stern + Stern + Stern + Stern + Stern);
		System.out.println(Stern + Stern + Stern + Stern + Stern + Stern + Stern);
		System.out.println(Stern + Stern);
		System.out.println(Stern + Stern);
		

		System.out.printf( "\n%s\n", Stern ); 	
		System.out.printf( "%s\n", Stern2 );
		System.out.printf( "%s\n", Stern3 );
		System.out.printf( "%s\n", Stern5 );
		System.out.printf( "%s\n", Stern2 );
		System.out.printf( "%s\n", Stern2 );

		System.out.println("\n Aufgabe 3 \n");
		
		System.out.printf( "%.2f\n", a );
		System.out.printf( "%.2f\n", b );
		System.out.printf( "%.2f\n", c );
		System.out.printf( "%.2f\n", d );
		System.out.printf( "%.2f\n", e );

	
	}

}
